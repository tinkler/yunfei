package wind

import (
	"database/sql"
	"sync"

	"golang.org/x/text/message"

	"gitee.com/tinkler/yunfei/ws/forge"
)

type Sleeve struct {
	storeMu sync.RWMutex
	store   forge.MemStore
	db      *sql.DB
	p       *message.Printer
}

func (s Sleeve) GetDB() *sql.DB {
	return s.db
}

func (s Sleeve) GetPrinter() *message.Printer {
	return s.p
}

func (s Sleeve) SetValue(key string, value interface{}) {
	s.storeMu.Lock()
	s.store[key] = value
	s.storeMu.Unlock()
}

func (s Sleeve) GetValue(key string) interface{} {
	s.storeMu.RLock()
	if value, has := s.store[key]; has {
		return value
	}
	s.storeMu.RUnlock()
	return nil
}
