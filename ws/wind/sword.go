package wind

import (
	"log"
	"os"

	"golang.org/x/text/message"

	"gitee.com/tinkler/yunfei/ws"
	"gitee.com/tinkler/yunfei/ws/forge"
)

type material struct {
	Log      *log.Logger
	ErrorLog *log.Logger
	Printer  *message.Printer
}

var m = material{}

func init() {
	sword := Sword{}
	ws.ForgeSword("wind", &sword)
}

// Sword sword with wind attrbute
type Sword struct{}

// Forge forge the wind sword
func (s Sword) Forge(config forge.YamlConf) {
	if config.Debug {
		m.Log = log.New(os.Stdout, "\x1b[0;33m[yunfei]\x1b[0m", log.LstdFlags)
	}
	m.ErrorLog = log.New(os.Stdout, "\x1b[0;31m[yunfei]\x1b[0m", log.LstdFlags)

}

// Logf debug log
func (s Sword) Logf(format string, a ...interface{}) {
	if m.Log != nil {
		m.Log.Printf(format, a...)
	}
}

// Errorf debug log
func (s Sword) Errorf(format string, a ...interface{}) {
	if m.ErrorLog != nil {
		m.ErrorLog.Printf(format, a...)
	}
}
