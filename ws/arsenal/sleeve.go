package arsenal

import (
	"database/sql"

	"golang.org/x/text/message"
)

type Sleeve interface {
	GetDB() *sql.DB
	GetPrinter() *message.Printer
	GetValue(key string) interface{}
	SetValue(key string, value interface{})
}
