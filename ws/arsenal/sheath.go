package arsenal

import (
	"database/sql"
)

type Sheath interface {
	CloseDB()
	GetDB() *sql.DB
	Use(name string) Sleeve
	SetLang(lang string)
}
