package arsenal

import "gitee.com/tinkler/yunfei/ws/forge"

// Scabbard api response
type Scabbard interface {
	SetError(message string) forge.Response
	NewSheath(name string, conf forge.YamlConf) Sheath
}
