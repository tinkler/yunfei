package arsenal

import (
	"gitee.com/tinkler/yunfei/ws/forge"
)

// Sword the sword
type Sword interface {
	Logf(format string, a ...interface{})
	Errorf(format string, a ...interface{})
	Forge(conf forge.YamlConf)
}
