package forge

// MemDB memory db
type MemDB map[string]MemStore

// MemStore memory key value store
type MemStore map[string]interface{}
