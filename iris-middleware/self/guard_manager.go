package self

import "database/sql"

// GuardManager guard manager
type GuardManager struct {
	gh *GuardHelper
}

func newGuardManager(gh *GuardHelper) *GuardManager {
	return &GuardManager{gh: gh}
}

// CreateRole create postgreSQL role
func (gm *GuardManager) CreateRole(rolename string) bool {
	return gm.gh.DefendPG(func(pg *sql.DB) error {

		return nil
	})
}
