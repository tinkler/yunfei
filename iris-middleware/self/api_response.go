package self

import (
	"github.com/kataras/iris"
)

var Success = func(ctx iris.Context) {
	ctx.JSON(iris.Map{"status": 200, "message": ctx.Values().GetString("message"), "data": ctx.Values().Get("data")})
}
