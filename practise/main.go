package main

import (
	"fmt"

	"gitee.com/yunind/baize/servers/baize/lib/target"
)

func main() {
	eo := target.NewEventObservable()
	// eo.Listen("goods", "supplier")
	eo.Dispatch("goods", target.SyncInstall, 5)
	eo.On("supplier", func(id int64, method target.SyncMethod) {
		// if id != 8 && method != SyncInstall {
		// 	t.Fail()
		// }
		fmt.Printf("supplier callback finish id %d", id)
	})
	eo.On("goods", func(id int64, method target.SyncMethod) {
		// if id == 10 || method == SyncInstall {
		// 	t.Fail()
		// 	t.Log("on action after dispatch")
		// } else if id != 8 || method != SyncInstall {
		// 	t.Fail()
		// 	t.Log("cancel action before dispatch")
		// }
		fmt.Printf("goods callback finish id %d", id)
	})

	eo.Dispatch("supplier", target.SyncInstall, 6)
	eo.On("supplier", func(id int64, method target.SyncMethod) {
		// if id != 8 && method != SyncInstall {
		// 	t.Fail()
		// }
		fmt.Printf("supplier callback finish id %d", id)
	})

	eo.Dispatch("goods", target.SyncInstall, 7)
	eo.Dispatch("goods", target.SyncInstall, 8)
}
