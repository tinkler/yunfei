package main

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/wonderivan/logger"

	"github.com/lib/pq"
)

func doWork(db *sql.DB, work time.Time) {
	logger.Info("work date %v", work)
}

func getWork(db *sql.DB) {
	for {
		// get work from the database here
		var work time.Time
		err := db.QueryRow("SELECT current_date").Scan(&work)
		if err != nil {
			fmt.Println("call to get_work() failed: ", err)
			time.Sleep(10 * time.Second)
			continue
		}

		fmt.Println("starting work on ", work)
		go doWork(db, work)
		return
	}
}

func waitForNotification(l *pq.Listener) {
	select {
	case <-l.Notify:
		fmt.Println("received notification, new work available")
	case <-time.After(90 * time.Second):
		go l.Ping()
		// Check if there's more work available, just in case it takes
		// a while for the Listener to notice connection loss and
		// reconnect.
		fmt.Println("received no work for 90 seconds, checking for new work")
	}
}

func main() {
	var conninfo = "host=localhost port=5432 user=baize password=baize4105 dbname=baize sslmode=disable"

	db, err := sql.Open("postgres", conninfo)
	if err != nil {
		panic(err)
	}

	reportProblem := func(ev pq.ListenerEventType, err error) {
		if err != nil {
			fmt.Println(err.Error())
		}
	}

	minReconn := 10 * time.Second
	maxReconn := time.Minute
	listener := pq.NewListener(conninfo, minReconn, maxReconn, reportProblem)
	err = listener.Listen("getwork")
	if err != nil {
		panic(err)
	}

	fmt.Println("entering main loop")
	for {
		// process all available work before waiting for notifications
		getWork(db)
		waitForNotification(listener)
	}
}
