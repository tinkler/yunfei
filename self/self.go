package self

import (
	"fmt"
	"sync"

	"gitee.com/tinkler/yunfei/self/driver"
)

var (
	driversMu     sync.RWMutex
	drivers       = make(map[string]driver.Driver)
	defaultDriver = "pgself"
)

// Register makes a self driver available by the provided name.
func Register(name string, driver driver.Driver) {
	driversMu.Lock()
	defer driversMu.Unlock()
	if driver == nil {
		panic("self: Register driver is nil")
	}
	if _, dup := drivers[name]; dup {
		panic("self: Register called twice for driver " + name)
	}
	drivers[name] = driver
}

// SetDefaultDriver change the default driver.
func SetDefaultDriver(name string) {
	driversMu.RLock()
	defer driversMu.RUnlock()
	if _, has := drivers[name]; !has {
		panic(fmt.Sprintf("self: Driver name %s is not exist", name))
	}
	defaultDriver = name
}

type Options struct {
	UserModel      interface{}
	DataSourceName string
	DriverName     string
}
